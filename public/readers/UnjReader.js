"use strict";

const UnjReader = class {

	constructor(images, bone_names) {

		this.images = images;
		this.bone_names = bone_names;

		this.MEM = {
			materials : [],
			vertices : [],
			bones : [],
			groups : [],
			calls : []
		};

		this.ATTR = {
			pos : [],
			norm : [],
			clr : [],
			clr_a : [],
			diffuse_uv : [],
			uv1 : [],
			bone_index : [],
			bone_weight : []
		};
		
		this.materials = new Array();
		this.geometry = new THREE.BufferGeometry();

	}

	parse(arraybuffer) {

		if (arraybuffer.byteLength < 16) {
			return;
		}

		this.data = arraybuffer;
		this.view = new DataView(arraybuffer);

		const MAGIC_NUOB = 0x424f554e;
		const magic = this.view.getUint32(0x00, true);
		const length = this.view.getUint32(0x04, true);
		const header_ofs = this.view.getUint32(0x08, true);
		const three = this.view.getUint32(0x0c, true);

		if (magic !== MAGIC_NUOB) {
			return false;
		}

		if (length !== arraybuffer.byteLength - 8) {
			return false;
		}

		if (header_ofs > arraybuffer.byteLength) {
			return false;
		}

		if (three !== 3) {
			console.warn("omfg, this is not a three!!??");
		}

		this.readHeader(header_ofs);
		this.readBones();
		this.readVertices();
		this.readMaterials();
		this.readGroups();
		
		let mesh;
		const debug_mat = new THREE.MeshNormalMaterial({
			skinning : true
		});

		if(!this.MEM.bones.length) {
			mesh = new THREE.Mesh(this.geometry, debug_mat);
		} else {
			mesh = new THREE.SkinnedMesh(this.geometry, this.materials);
			const armSkeleton = new THREE.Skeleton(this.MEM.bones);
			const rootBone = armSkeleton.bones[0];
			mesh.add(rootBone);
			mesh.bind(armSkeleton);
		}

		return mesh;

	}

	readHeader(header_ofs) {
		
		const ofs = header_ofs;

		const unj_header_t = {
			center: {
				x: this.view.getFloat32(ofs + 0x00, true),
				y: this.view.getFloat32(ofs + 0x04, true),
				z: this.view.getFloat32(ofs + 0x08, true)
			},
			radius: this.view.getFloat32(ofs + 0x0c, true),
			material_count: this.view.getUint32(ofs + 0x10, true),
			material_ofs: this.view.getUint32(ofs + 0x14, true),
			vertex_group_count: this.view.getUint32(ofs + 0x18, true),
			vertex_group_ofs: this.view.getUint32(ofs + 0x1c, true),
			index_group_count: this.view.getUint32(ofs + 0x20, true),
			index_group_ofs: this.view.getUint32(ofs + 0x24, true),
			bone_count: this.view.getUint32(ofs + 0x28, true),
			bone_tree_depth: this.view.getUint32(ofs + 0x2c, true),
			bone_ofs: this.view.getUint32(ofs + 0x30, true),
			unkown_1: this.view.getUint32(ofs + 0x34, true),
			draw_count: this.view.getUint32(ofs + 0x38, true),
			draw_ofs: this.view.getUint32(ofs + 0x3c, true)
		};
		
		this.header = unj_header_t;
	}

	readVertices() {
		
		const groups = [];
		let ofs = this.header.vertex_group_ofs;

		// Read the uv count and get pointer to vertex list descriptor

		for (let i = 0; i < this.header.vertex_group_count; i++) {
			const unj_vertex_group_t = {
				index : i,
				uv_count: this.view.getUint32(ofs + 0, true),
				list_ofs: this.view.getUint32(ofs + 4, true)
			}
			groups.push(unj_vertex_group_t);
			ofs += 8;
		}

		// We seek to the vertex list descriptor and read each one
		
		let implied_bone = 0;
		groups.forEach( group => {

			ofs = group.list_ofs;

			const unj_vertex_list_t = {
				unkown_1: this.view.getUint32(ofs + 0x00, true),
				vertex_format: this.view.getUint32(ofs + 0x04, true),
				unknown_2: this.view.getUint32(ofs + 0x08, true),
				unknown_3: this.view.getUint8(ofs + 0x0c),
				vertex_length: this.view.getUint8(ofs + 0x0d),
				nop: this.view.getUint16(ofs + 0x0e, true),
				vertex_count_ofs: this.view.getUint32(ofs + 0x10, true),
				vertex_list_ofs: this.view.getUint32(ofs + 0x14, true),
				bone_binding_ofs: this.view.getUint32(ofs + 0x18, true),
				bone_binding_count: this.view.getUint32(ofs + 0x1c, true),
				total_vertex_count: this.view.getUint32(ofs + 0x20, true),
				unknown_4: this.view.getUint32(ofs + 0x24, true),
				unknown_5: this.view.getUint32(ofs + 0x28, true),
				vertex_scale: this.view.getFloat32(ofs + 0x2c, true),
				bone_id : implied_bone++
			};

			console.log(unj_vertex_list_t);

			ofs = unj_vertex_list_t.vertex_count_ofs;
			unj_vertex_list_t.vertex_count = this.view.getUint32(ofs, true);
			
			ofs = unj_vertex_list_t.bone_binding_ofs;
			group.bones = new Array();
			console.log("Binding count: %d", unj_vertex_list_t.bone_binding_count);
			for (let i = 0; i < unj_vertex_list_t.bone_binding_count; i++) {
				group.bones[i] = this.view.getUint32(ofs, true);
				ofs += 4;
			}

			for (let key in unj_vertex_list_t) {
				group[key] = unj_vertex_list_t[key];
			}

		});
		
		groups.forEach(group => {

			const UINT8 = 1;
			const INT8 = 1;
			const INT16 = 2;
			const FLOAT = 3;

			const bones = group.bones;
			const vertices = new Array(group.vertex_count);
			const scale = group.vertex_scale;
			const format = group.vertex_format;
			const stride = group.vertex_length;
			const uv_count = group.uv_count;

			const uvFormat = (format & 0x3);
			const colorFormat = (format >> 2) & 0x7;
			const normalFormat = (format >> 5) & 0x3;
			const positionFormat = (format >> 7) & 0x3;
			const weightFormat = (format >> 9) & 0x3;

			let ofs = group.vertex_list_ofs;
			for (let i = 0; i < vertices.length; i++) {

				const vertex = {};
				const start = ofs;

				vertex.weight = [];
				switch (weightFormat) {
					case UINT8:
					for (let j = 0; j < bones.length; j++) {
						vertex.weight[j] = {
							bone_id: bones[j],
							weight: this.view.getUint8(ofs) / 0x7f
						}
						ofs++;
					}
					break;
					case INT16:
					for (let j = 0; j < bones.length; j++) {
						vertex.weight[j] = {
							bone_id: bones[j],
							weight: this.view.getInt16(ofs, true) / 0x7fff
						}
						ofs += 2;
					}
					break;
					case FLOAT:
					for (let j = 0; j < bones.length; j++) {
						vertex.weight[j] = {
							bone_id: bones[j],
							weight: this.view.getFloat32(ofs, true)
						}
						ofs += 4;
					}
					break;
					default:
						this.backup_weight = true;
					break;
				}

				// Read uv values
 
				vertex.uvs = [];
				switch (uvFormat) {
					case INT8:
						for (let j = 0; j < uv_count; j++) {
							vertex.uvs[j] = {
								u: this.view.getInt8(ofs + 0) / 0x7f,
								v: this.view.getInt8(ofs + 1) / 0x7f
							}
							ofs += 2;
						}
						break;
					case INT16:
						if (ofs % 2) {
							ofs += 2 - (ofs % 2);
						}
 
						for (let j = 0; j < uv_count; j++) {
							vertex.uvs[j] = {
								u: this.view.getInt16(ofs + 0, true) / 0x7fff,
								v: this.view.getInt16(ofs + 2, true) / 0x7fff
							}
							ofs += 4;
						}
						break;
					case FLOAT:
						if (ofs % 4) {
							ofs += 4 - (ofs % 4);
						}
 
						for (let j = 0; j < uv_count; j++) {
							vertex.uvs[j] = {
								u: this.view.getFloat32(ofs + 0, true),
								v: this.view.getFloat32(ofs + 4, true)
							}
							ofs += 8;
						}
						break;
				}
 
				// Read colors (we assume ARGB4444)
 				
				vertex.color = { r:1, g:1, b:1, a:1 };

				if (colorFormat) {
					if (ofs % 2) {
						ofs += 2 - (ofs % 2);
					}
					let byte1 = this.view.getUint8(ofs + 0);
					let byte2 = this.view.getUint8(ofs + 1);
					ofs += 2;
					vertex.color = {
						r: ((byte1 >> 0) & 0xf) / 0x0f,
						g: ((byte1 >> 4) & 0xf) / 0x0f,
						b: ((byte2 >> 0) & 0xf) / 0x0f,
						a: ((byte2 >> 4) & 0xf) / 0x0f
					};
				}
 
				// Read normals
 
				switch (normalFormat) {
					case UINT8:
						vertex.normal = {
							x: this.view.getUint8(ofs + 0) / 0x7f,
							y: this.view.getUint8(ofs + 1) / 0x7f,
							z: this.view.getUint8(ofs + 2) / 0x7f
						}
						ofs += 3;
						break;
					case INT16:
						if (ofs % 2) {
							ofs += 2 - (ofs % 2);
						}
						vertex.normal = {
							x: this.view.getInt16(ofs + 0, true) / 0x7fff,
							y: this.view.getInt16(ofs + 2, true) / 0x7fff,
							z: this.view.getInt16(ofs + 4, true) / 0x7fff
						}
						ofs += 6;
						break;
					case FLOAT:
						if (ofs % 4) {
							ofs += 4 - (ofs % 4);
						}
 
						vertex.normal = {
							x: this.view.getFloat32(ofs + 0, true),
							y: this.view.getFloat32(ofs + 4, true),
							z: this.view.getFloat32(ofs + 8, true)
						}
						ofs += 12;
						break;
				}
 
				// Read Position
 
				switch (positionFormat) {
					case UINT8:
						vertex.x = this.view.getUint8(ofs + 0) / 0x7f * scale;
						vertex.y = this.view.getUint8(ofs + 1) / 0x7f * scale;
						vertex.z = this.view.getUint8(ofs + 2) / 0x7f * scale;
						ofs += 3;
						break;
					case INT16:
						if (ofs % 2) {
							ofs += 2 - (ofs % 2);
						}
						vertex.x = this.view.getInt16(ofs + 0, true) / 0x7fff * scale;
						vertex.y = this.view.getInt16(ofs + 2, true) / 0x7fff * scale;
						vertex.z = this.view.getInt16(ofs + 4, true) / 0x7fff * scale;
						ofs += 6;
						break;
					case FLOAT:
						if (ofs % 4) {
							ofs += 4 - (ofs % 4);
						}
						vertex.x = this.view.getFloat32(ofs + 0, true) * scale;
						vertex.y = this.view.getFloat32(ofs + 4, true) * scale;
						vertex.z = this.view.getFloat32(ofs + 8, true) * scale;
						ofs += 12;
						break;
				}
 
				const end = ofs;
				if (end - start !== stride) {
					throw new Error("Phission Mailed, better luck next time");
				}
 
				vertices[i] = vertex;
			}
			
			group.vertices = vertices;

			// End read vertex List

		});

		// Last convert the triangle strips into normal triangles

		groups.forEach( group => {
			
			// Add the vertices to the instance memory
			
			const src = group.vertices;
			const vertex_start = this.MEM.vertices.length;
			let vertex_length = 0;

			for(let i = 0; i < src.length; i++) {
				// Create a key and index each on of the vertices (locally)
				vertex_length++;
			}
	
			// Add convert strips to indexed triangles and save range

			const index_start = this.MEM.vertices.length;
			let index_length = 0;
			
			let dir = true;
			for(let i = 0; i < src.length - 2; i++) {
				dir = !dir;

				let a, b, c;
				if(!dir) {
					a = src[i + 0];
					b = src[i + 1];
					c = src[i + 2];
				} else {
					a = src[i + 1];
					b = src[i + 0];
					c = src[i + 2];
				}

				const a_key = [a.x.toFixed(3), a.y.toFixed(3), a.z.toFixed(3)].join();
				const b_key = [b.x.toFixed(3), b.y.toFixed(3), b.z.toFixed(3)].join();
				const c_key = [c.x.toFixed(3), c.y.toFixed(3), c.z.toFixed(3)].join();

				if(a_key === b_key || b_key === c_key || c_key === a_key) {
					continue;
				}

				vertex_length += 3;
				this.MEM.vertices.push(a, b, c);

			}

			this.MEM.groups[group.index] = {
				start : vertex_start,
				length : this.MEM.vertices.length - vertex_start
			};

		});

		// Convert list of vertices into a list of attributes

		this.MEM.vertices.forEach(v => {
			
			this.ATTR.pos.push(v.x, v.y, v.z);

			for(let i = 0; i < 4; i++) {
				if(!v.weight[i]) {
					this.ATTR.bone_index.push(0);
					this.ATTR.bone_weight.push(0);
				} else {
					this.ATTR.bone_index.push(v.weight[i].bone_id);
					this.ATTR.bone_weight.push(v.weight[i].weight);
				}
			}

			this.ATTR.clr.push(v.color.r, v.color.g, v.color.b);
			this.ATTR.clr_a.push(v.color.a);

			if(v.uvs[0]) {
				this.ATTR.diffuse_uv.push(v.uvs[0].u, v.uvs[0].v);
			} else {
				this.ATTR.diffuse_uv.push(0,0);
			}

		});

		const pos = new Float32Array(this.ATTR.pos);
		this.geometry.setAttribute('position', new THREE.BufferAttribute(pos, 3));

		const skinIndex = new Uint16Array(this.ATTR.bone_index);
		const skinWeight = new Float32Array(this.ATTR.bone_weight);
		this.geometry.setAttribute('skinIndex', new THREE.BufferAttribute(skinIndex, 4));
		this.geometry.setAttribute('skinWeight', new THREE.BufferAttribute(skinWeight, 4));

		const color = new Float32Array(this.ATTR.clr);
		const color_alpha = new Float32Array(this.ATTR.clr_a);
		this.geometry.setAttribute('color', new THREE.BufferAttribute(color, 3));
		this.geometry.setAttribute('color_alpha', new THREE.BufferAttribute(color_alpha, 1));

		const diffuse = new Float32Array(this.ATTR.diffuse_uv);
		this.geometry.setAttribute('uv', new THREE.BufferAttribute(diffuse, 2));

		this.geometry.computeBoundingBox();
		this.geometry.computeBoundingSphere();
		this.geometry.computeVertexNormals();

	}

	readBones() {

		console.log(this.bone_names);

		let str = "";
		let ofs = this.header.bone_ofs;
		
		this.bone_map = [];
		for(let i = 0; i < this.header.bone_count - 1; i++) {
			
			const unj_bone_t = {
				flags : this.view.getUint32(ofs + 0x00, true),
				bone_id : this.view.getInt16(ofs + 0x04, true),
				parent_id : this.view.getInt16(ofs + 0x06, true), 
				child_id : this.view.getInt16(ofs + 0x08, true),
				sibling_id : this.view.getInt16(ofs + 0x0a, true),
				pos : [
					this.view.getFloat32(ofs + 0x0c, true),
					this.view.getFloat32(ofs + 0x10, true),
					this.view.getFloat32(ofs + 0x14, true)
				],
				rot : [
					this.view.getInt32(ofs + 0x18, true) / 0x8000 * Math.PI,
					this.view.getInt32(ofs + 0x1c, true) / 0x8000 * Math.PI,
					this.view.getInt32(ofs + 0x20, true) / 0x8000 * Math.PI
				],
				scl : [
					this.view.getFloat32(ofs + 0x24, true),
					this.view.getFloat32(ofs + 0x28, true),
					this.view.getFloat32(ofs + 0x2c, true)
				]
			}
			
			const bindMat = new THREE.Matrix4();
			bindMat.set(
				this.view.getFloat32(ofs + 0x30, true),
				this.view.getFloat32(ofs + 0x34, true),
				this.view.getFloat32(ofs + 0x38, true),
				this.view.getFloat32(ofs + 0x3c, true),
				this.view.getFloat32(ofs + 0x40, true),
				this.view.getFloat32(ofs + 0x44, true),
				this.view.getFloat32(ofs + 0x48, true),
				this.view.getFloat32(ofs + 0x4c, true),
				this.view.getFloat32(ofs + 0x50, true),
				this.view.getFloat32(ofs + 0x54, true),
				this.view.getFloat32(ofs + 0x58, true),
				this.view.getFloat32(ofs + 0x5c, true),
				this.view.getFloat32(ofs + 0x60, true),
				this.view.getFloat32(ofs + 0x64, true),
				this.view.getFloat32(ofs + 0x68, true),
				this.view.getFloat32(ofs + 0x6c, true)
			);
			bindMat.transpose();
			const childWorldMat = new THREE.Matrix4();
			childWorldMat.getInverse(bindMat, true);
			ofs += 0x90;

			const bone = new THREE.Bone();
			bone.name = this.bone_names[i];
			bone.userData = {
				id : unj_bone_t.bone_id
			}

			if(unj_bone_t.bone_id >= 0) {
				this.bone_map[unj_bone_t.bone_id] = i;
			}
			
			console.log("%d) [%d] %s", i, unj_bone_t.bone_id, bone.name);

			let localMatrix = new THREE.Matrix4();
			const parentBone = this.MEM.bones[unj_bone_t.parent_id];

			if(parentBone) {
				localMatrix.getInverse(parentBone.matrixWorld, true);
				parentBone.add(bone);
			}
			localMatrix.multiply(childWorldMat);
			bone.applyMatrix(localMatrix);
			
			bone.updateMatrix();
			bone.updateMatrixWorld();
			this.MEM.bones.push(bone);

		}


	}

	readMaterials () {

		let ofs = this.header.material_ofs;
		const mat_list = new Array();

		for(let i = 0; i < this.header.material_count; i++) {
			
			const unj_matlist_t = {
				diffuse_texture_count : this.view.getUint8(ofs + 0x00),
				effect_texture_count : this.view.getUint8(ofs + 0x01),
				nop : this.view.getUint16(ofs + 0x02),
				material_ofs : this.view.getUint32(ofs + 0x04, true)
			};
			
			ofs += 0x08;
			mat_list[i] = unj_matlist_t;

		}

		mat_list.forEach(mat => {
			
			ofs = mat.material_ofs;
			ofs += 8;
			
			const color = {
				emissive : {},
				ambient : {},
				diffuse : {},
				specular : {}
			};

			const texture_props = [];
			const textures = [];

			// Read the texture mapping type

			for (let i = 0; i < mat.diffuse_texture_count; i++) {
				
				texture_props[i] = {};
				const byte = this.view.getUint8(ofs + 1);
				ofs += 4;

				switch (byte) {
				case 1:
					texture_props[i].mappingType = "env";
					break;
				default:
					texture_props[i].mappingType = "uv";
					break;
				}

			}

			// Read the color properties for the material

			 for (let i = 0; i < 6; i++) {
				
				const a = this.view.getUint8(ofs + 0);
				const b = this.view.getUint8(ofs + 1);
				const c = this.view.getUint8(ofs + 2);
				const d = this.view.getUint8(ofs + 3);

				const tmp = new Uint8Array([0, a, b, c]).buffer;
				const view = new DataView(tmp);
				const float_val = view.getFloat32(0, true);

				ofs += 4;

				switch(d) {
				case 0x54:
					color.emissive.r = a / 255;
					color.emissive.g = b / 255;
					color.emissive.b = c / 255;
					break;
				case 0x55:
					color.ambient.r = a / 255;
					color.ambient.g = b / 255;
					color.ambient.b = c / 255;
					break;
				case 0x56:
					color.diffuse.r = a / 255;
					color.diffuse.g = b / 255;
					color.diffuse.b = c / 255;
					break;
				case 0x57:
					color.specular.r = a / 255;
					color.specular.g = b / 255;
					color.specular.b = c / 255;
					break;
				case 0x58:
					color.emissive.a = a / 255;
					color.ambient.a = a / 255;
					color.diffuse.a = a / 255;
					break;
				case 0x58:
					color.speculat.coef = float_val;
					break;
				}
			}
			
			// Skipp over a range of zeros

			ofs += 0x18;

			// Then we read all of the texture properties

			for (let i = 0; i < mat.diffuse_texture_count; i++) {
				
				let a, b, c, d;
				const prop = texture_props[i];

				do {
					
					a = this.view.getUint8(ofs + 0);
					b = this.view.getUint8(ofs + 1);
					c = this.view.getUint8(ofs + 2);
					d = this.view.getUint8(ofs + 3);
					ofs += 4;

					const tmp = new Uint8Array([0, a, b, c]).buffer;
					const view = new DataView(tmp);
					const float_val = view.getFloat32(0, true);

					switch(d) {
					case 0x21:
						prop.disableLighting = (a === 1);
						break;
					case 0x22:
						prop.alphaTestEnabled = (a === 1);
						break;
					case 0x23:
						prop.zTestEnabled = (a === 1);
						break;
					case 0x24:
						prop.stencilTestEnabled = (a === 1);
						break;
					case 0x27:
						prop.colorTestEnabled = (a === 1);
						break;
					case 0x28:
						prop.logicOpEnabled = (a === 1);
						break;
					case 0x48:
						prop.scaleU = float_val;
						break;
					case 0x49:
						prop.scaleV = float_val;
						break;
					case 0x4a:
						prop.offsetU = float_val;
						break;
					case 0x48:
						prop.offsetV = float_val;
						break;
					case 0x5e:
						prop.diffuseEnabled = a < 2;
						break;
					case 0xc7:
						prop.clampU = (a & 1) !== 0;
						prop.clampV = (b & 1) !== 0;
						break;
					case 0xc9:
						prop.textureFunction = (a & 7);
						prop.textureFunctionUsesAlpha = (b === 1);
						break;
					case 0xdb:
						prop.alphaFunction = a;
						prop.alphaRef = b;
						break;
					case 0xde:
						prop.zTestFunction = a;
						break;
					case 0xdf:
						prop.blendMode = b;
						prop.blendFactorA = a;
						prop.blendFactorB = a;
						break;
					case 0xe0:
						prop.blendFixedA = (c << 16) | (b << 8) | a;
						break;
					case 0xe1:
						prop.blendFixedB = (c << 16) | (b << 8) | a;
						break;
					case 0xe6:
						prop.logicOp = a;
						break;
					case 0xe7:
						prop.zWriteDisabled = a ? true : false;
						break;
					case 0xe8:
						prop.maskRGB = (c << 16) | (b << 8) | a;
						break;
					case 0xe9:
						prop.maskAlpha = (c << 16) | (b << 8) | a;
						break;
					}

				} while(d !== 0x0b)

			}
	
			if(!this.images) {
				let debug_mat = new THREE.MeshBasicMaterial({
					color : Math.floor(Math.random() * 0xffffff),
					skinning : true
				});
				this.materials.push(debug_mat);
				return;
			}

			// Last we get the texture id

			for (let i = 0; i < mat.diffuse_texture_count; i++) {
				
				const texture_id = this.view.getUint32(ofs, true);
				ofs += 4;
				textures[i] = new THREE.Texture(this.images[texture_id]);
				textures[i].needsUpdate = true;
				textures[i].flipY = false;

			}

			const basic_mat = new THREE.MeshPhongMaterial({
				skinning : true,
				map : textures[0]
			});
			
			this.materials.push(basic_mat);

		});
		
	}

	readGroups() {

		let ofs = this.header.draw_ofs;
		const groups = [];

		// The draw count in the header should only ever be one
		// But we'll implement this as a for-loop to be on the safe side

		for (let i = 0; i < this.header.draw_count; i++) {
			
			const unj_drawgroups_t = {
				unknown_byte_1: this.view.getUint8(ofs + 0x00),
				unknown_byte_2: this.view.getUint8(ofs + 0x01),
				unknown_short_1: this.view.getUint16(ofs + 0x02, true),
				direct_draw_count: this.view.getUint32(ofs + 0x04, true),
				direct_draw_ofs: this.view.getUint32(ofs + 0x08, true),
				indexed_draw_count: this.view.getUint32(ofs + 0x0c, true),
				indexed_draw_ofs: this.view.getUint32(ofs + 0x10, true)
			};

			groups[i] = unj_drawgroups_t;
			ofs += 0x14;

		}
	
		// Then we read each one of the direct draws calls and save to memory

		console.log("START GROUPS!!!");

		groups.forEach(group => {

			ofs = group.direct_draw_ofs;
			for (let i = 0; i < group.direct_draw_count; i++) {
				
				const unj_direct_call_t = {
					center: {
						x: this.view.getFloat32(ofs + 0x00, true),
						y: this.view.getFloat32(ofs + 0x04, true),
						z: this.view.getFloat32(ofs + 0x08, true)
					},
					radius: this.view.getFloat32(ofs + 0x0c, true),
					top_level_bone: this.view.getUint32(ofs + 0x10, true),
					unknown_int1: this.view.getUint32(ofs + 0x14, true),
					mat_id: this.view.getUint32(ofs + 0x18, true),
					group_id : this.view.getUint32(ofs + 0x1c, true),
					unknown_int2: this.view.getUint32(ofs + 0x20, true)
				}

				console.log(unj_direct_call_t);

				this.MEM.calls.push(unj_direct_call_t);
				ofs += 0x24;
			}

		});

		console.log("END GROUPS!!!!");
		
		// Then we need to go through the list and collapse it down
		// in a shorter list of draw calls for the same material
		
		let last;
		this.ATTR.bone_index = [];
		this.ATTR.bone_weight = [];

		for(let i = 0; i < this.MEM.calls.length; i++) {
			
			const mat_id = this.MEM.calls[i].mat_id;
			const group_id = this.MEM.calls[i].group_id;
			const v_group = this.MEM.groups[group_id];
			
			for(let k = 0; k < v_group.length; k++) {
				this.ATTR.bone_index.push(this.MEM.calls[i].top_level_bone);
				this.ATTR.bone_weight.push(1);
			}

			if(!last) {
				last = {
					start : v_group.start,
					count : v_group.length, 
					materialIndex : mat_id
				};
				continue;
			}

			let update = false;
			if(last.materialIndex !== mat_id) {
				update = true;
			} else if(last.start + last.count !== v_group.start) {
				update = true;
			}

			if(!update) {
				last.count += v_group.length;
				continue;
			}
			
			this.geometry.addGroup(last.start, last.count, last.materialIndex);

			last = {
				start : v_group.start,
				count : v_group.length, 
				materialIndex : mat_id
			};

		}
		
		this.geometry.addGroup(last.start, last.count, last.materialIndex);
		console.log(this.geometry);
		
		if(this.backup_weight) {
			const skinIndex = new Uint16Array(this.ATTR.bone_index);
			const skinWeight = new Float32Array(this.ATTR.bone_weight);
			this.geometry.setAttribute('skinIndex', new THREE.BufferAttribute(skinIndex, 1));
			this.geometry.setAttribute('skinWeight', new THREE.BufferAttribute(skinWeight, 1));
		}

	}



};
