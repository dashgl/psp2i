"use strict";

class UnmReader {

	constructor(mesh) {

		if(!mesh) {
			throw new Error("must provide skinned mesh");
		}

		this.mesh = mesh;
		this.bones = this.mesh.skeleton.bones;

		if(!this.mesh.geometry.animations) {
			this.mesh.geometry.animations = [];
		}

	}

	parse(arraybuffer, name) {

		if (arraybuffer.byteLength < 16) {
			return false;
		}

		// We reset the instance memory

		this.MEM = {}
		this.MEM.data = arraybuffer;
		this.MEM.view = new DataView(arraybuffer);

		const MAGIC_NUMO = 0x4f4d554e;
		const magic = this.MEM.view.getUint32(0x00, true);
		const length = this.MEM.view.getUint32(0x04, true);
		const header_ofs = this.MEM.view.getUint32(0x08, true);

		if (magic !== MAGIC_NUMO) {
			return false;
		}

		if (length !== arraybuffer.byteLength - 8) {
			return false;
		}

		// Read the header

		this.readHeader(header_ofs);
		this.MEM.max_len = 0;

		this.MEM.anim = {
			name : name.split(".").shift(),
			fps : this.MEM.header.fps,
			length : this.MEM.header.length,
			hierarchy : []
		}

		for(let i = 0; i < this.bones.length; i++) {
			this.MEM.anim.hierarchy[i] = {
				parent : i - 1,
				keys : []
			}
		}

		// Read the motion table and key frames

		this.readMotion();
		this.readRootFrames();
		this.readBoneFrames();

		this.MEM.anim.length = this.MEM.max_len;
		console.log("Max length: %s", this.MEM.max_len);

		// Clean up memory

		const clip = THREE.AnimationClip.parseAnimation(this.MEM.anim, this.bones);
		clip.optimize();
		console.log(clip);

		this.mesh.geometry.animations.push(clip);
		delete this.MEM;
	
		return true;

	}

	readHeader(ofs) {
		
		const unm_header_t = {
			flag_a : this.MEM.view.getUint16(ofs + 0x00, true),
			flag_b : this.MEM.view.getUint16(ofs + 0x02, true),
			zero : this.MEM.view.getUint32(ofs + 0x04, true),
			frames : this.MEM.view.getFloat32(ofs + 0x08, true),
			motion_count : this.MEM.view.getUint32(ofs + 0x0c, true),
			motion_ofs : this.MEM.view.getUint32(ofs + 0x10, true),
			fps : this.MEM.view.getFloat32(ofs + 0x14, true)
		}

		this.MEM.header = unm_header_t;
		this.MEM.header.length = unm_header_t.frames / unm_header_t.fps;

	}

	readMotion() {

		this.MEM.motion = [];

		let ofs = this.MEM.header.motion_ofs;
		const motion_count = this.MEM.header.motion_count;

		const bone_ids = [];

		for(let i = 0; i < motion_count; i++) {
			
			const unm_motion_t = {
				keyframe_flag : this.MEM.view.getUint32(ofs + 0x00, true),
				short_a : this.MEM.view.getUint16(ofs + 0x04, true),
				short_b : this.MEM.view.getUint16(ofs + 0x06, true),
				bone_id : this.MEM.view.getUint32(ofs + 0x08, true),
				unknown_a : this.MEM.view.getFloat32(ofs + 0x0c, true),
				unknown_b : this.MEM.view.getFloat32(ofs + 0x10, true),
				unknown_c : this.MEM.view.getFloat32(ofs + 0x14, true),
				unknown_d : this.MEM.view.getFloat32(ofs + 0x18, true),
				keyframe_count : this.MEM.view.getUint32(ofs + 0x1c, true),
				keyframe_size : this.MEM.view.getUint32(ofs + 0x20, true),
				keyframe_ofs : this.MEM.view.getUint32(ofs + 0x24, true),
			}
			
			if(bone_ids.indexOf(unm_motion_t.bone_id) === -1) {
				bone_ids.push(unm_motion_t.bone_id);
			}

			switch(unm_motion_t.keyframe_flag) {
			case 0x0701:
				unm_motion_t.type = "pos";
				break;
			case 0x3812:
				unm_motion_t.type = "rot";
				break;
			default:
				console.log("unknown motion type: 0x%s", unm_motion_t.keyframe_flag.toString(16));
				break;
			}

			this.MEM.motion.push(unm_motion_t);
			ofs += 0x28;

		}

		console.log(bone_ids);

	}

	readRootFrames() {

		const pos_t = this.MEM.motion.shift();
		const rot_t = this.MEM.motion.shift();
		console.log(rot_t);

		const keys = [];
		const bone = this.bones[0];

		// Read root bone position

		let ofs = pos_t.keyframe_ofs;
		for(let i = 0; i < pos_t.keyframe_count; i++) {

			const unm_posframe_t = {
				frame : this.MEM.view.getFloat32(ofs + 0x00, true),
				pos_x : this.MEM.view.getFloat32(ofs + 0x04, true),
				pos_y : this.MEM.view.getFloat32(ofs + 0x08, true),
				pos_z : this.MEM.view.getFloat32(ofs + 0x0c, true)
			}
			
			ofs += 0x10;
			
			/*
			keys[parseInt(unm_posframe_t.frame)] = {
				time : unm_posframe_t.frame / this.MEM.header.fps,
				scl : [1,1,1],
				pos : [
					unm_posframe_t.pos_x,
					unm_posframe_t.pos_y,
					unm_posframe_t.pos_z
				],
				rot : [ 0,0,0,1 ]
			};
			*/

		}
		
		// Read root bone rotation
		
		console.log(rot_t);

		ofs = rot_t.keyframe_ofs;
		for(let i = 0; i < rot_t.keyframe_count; i++) {

			const unm_rotframe_t = {
				frame : this.MEM.view.getUint16(ofs + 0x00, true),
				rot_x : this.MEM.view.getUint16(ofs + 0x02, true),
				rot_y : this.MEM.view.getUint16(ofs + 0x04, true),
				rot_z : this.MEM.view.getUint16(ofs + 0x06, true)
			}
			
			const euler = new THREE.Euler(
				unm_rotframe_t.rot_x * Math.PI / 0x8000,
				unm_rotframe_t.rot_y * Math.PI / 0x8000,
				unm_rotframe_t.rot_z * Math.PI / 0x8000
			);

			const quat = new THREE.Quaternion();
			quat.setFromEuler(euler);

			ofs += 0x08;

			//keys[parseInt(unm_rotframe_t.frame)].rot = quat.toArray();
			//keys[parseInt(unm_rotframe_t.frame)].rot = [0,0,0,1];
			
		}
		
		// Add keys to animation
		for(let i = 0; i < keys.length; i++) {
			this.MEM.anim.hierarchy[0].keys.push(keys[i]);
		}


	}

	readBoneFrames() {
		
		console.log(this.bones);

		this.MEM.motion.forEach(mot => {
			
			if(mot.bone_id !== 2) {
				return;
			}

			if(mot.keyframe_count === 1) {
				return;
			}

			let ofs = mot.keyframe_ofs;
			for(let i = 0; i < mot.keyframe_count; i++) {
				
				const unm_rotframe_t = {
					index : this.MEM.view.getInt16(ofs + 0x00, true),
					rot_x : this.MEM.view.getInt16(ofs + 0x02, true) * Math.PI / 0x8000,
					rot_y : this.MEM.view.getInt16(ofs + 0x04, true) * Math.PI / 0x8000,
					rot_z : this.MEM.view.getInt16(ofs + 0x06, true) * Math.PI / 0x8000
				}
				ofs += 8;
				
				const euler = new THREE.Euler(
					unm_rotframe_t.rot_x,
					unm_rotframe_t.rot_y,
					unm_rotframe_t.rot_z
				);
				
				const rot = new THREE.Matrix4();

				const mx = new THREE.Matrix4();
				const my = new THREE.Matrix4();
				const mz = new THREE.Matrix4();

				mx.makeRotationX (unm_rotframe_t.rot_x);
				my.makeRotationY (unm_rotframe_t.rot_y);
				mz.makeRotationZ (unm_rotframe_t.rot_z);

				rot.multiply(mx);
				rot.multiply(my);
				rot.multiply(mz);

				const quat = new THREE.Quaternion();
				// quat.setFromEuler(euler);
				quat.setFromRotationMatrix (rot);

				let index = -1;
				for(let i = 0; i < this.bones.length; i++) {
					if(this.bones[i].userData.id !== mot.bone_id) {
						continue;
					}
					index = i;
					break;
				}

				index = 2;
				const time = unm_rotframe_t.index / this.MEM.header.fps;
				this.MEM.anim.hierarchy[index].keys.push({
					time : time,
					scl : [1,1,1],
					rot : quat.toArray(),
					// rot : [0 , 0 , 0, 1],
					pos : [
						this.bones[index].position.x,
						this.bones[index].position.y,
						this.bones[index].position.z
					]
				});

				if(time > this.MEM.max_len) {
					this.MEM.max_len = time;
				}
				
			}

		});

	}

}
