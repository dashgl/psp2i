"use strict";

class UnaReader {

	constructor() {

	}

	parse(arraybuffer) {

		const view = new DataView(arraybuffer);
		const headerOfs = view.getUint32(0x08, true);
		console.log("Header offset: 0x%s", headerOfs.toString(16));

		const boneCount = view.getUint32(headerOfs + 0x04, true);
		const names = new Array(boneCount);

		let ofs = view.getUint32(headerOfs + 0x08, true);
		for(let i = 0; i < boneCount; i++) {
			const id = view.getUint32(ofs + 0x00, true);
			let start = view.getUint32(ofs + 0x04, true);
			ofs += 8;

			let ch;
			let name = "";
			while( (ch = view.getUint8(start++)) !== 0) {
				name += String.fromCharCode(ch);
			}
			name.replace(/ /g, '_');
			names[id] = name;
		}

		return names;
	}

}
