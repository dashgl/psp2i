"use strict";

const UvrReader = class {

	constructor() {

	}

	getTextureList (data) {

		const view = new DataView(data);

		const magic = view.getUint32(0, true);
		const length = view.getUint32(4, true);
		const header_ofs = view.getUint32(8, true);
	
		const stride = 0x14;
		const tex_count = view.getUint32(header_ofs + 0, true);
		let offset = view.getUint32(header_ofs + 4, true);

		let tex_list = new Array(tex_count);
		for(let i = 0; i < tex_count; i++) {
			let ofs = view.getUint32(offset + 4, true);
			offset += stride;
		
			let ch;
			let name = "";
			while(ch = view.getUint8(ofs++)) {
				name += String.fromCharCode(ch);
			}
			tex_list[i] = name;
		}

		return tex_list;

	}

	parse (data) {

		this.view = new DataView(data);
		const GBIX = this.view.getUint32(0x00, true);
		const UVRT = this.view.getUint32(0x10, true);
		const LEN  = this.view.getUint32(0x14, true) - 8;

		this.pixel = this.view.getUint8(0x18);
		this.format = this.view.getUint8(0x19);
		this.width = this.view.getUint16(0x1c, true);
		this.height = this.view.getUint16(0x1e, true);

		this.canvas = document.createElement("canvas");
		this.ctx = this.canvas.getContext('2d');
		this.canvas.width = this.width;
		this.canvas.height = this.height;

		switch(this.format) {
		case 0x80:
		case 0x81:
			this.readDirect();
			break;
		case 0x86:
		case 0x88:
		case 0x89:
			this.readIndex16();
			break;
		case 0x8a:
		case 0x8c:
			this.readIndex256();
			break;
		}

		return this.canvas;

	}

	readDirect() {
		
		let block_size = this.pixel !== 3 ? 8 : 4;
		
		let ofs = 0x20;
		for (let y = 0; y < this.height; y += 8) {
			for (let x = 0; x < this.width; x += block_size) {
				for (let y2 = 0; y2 < 8; y2++) {
					for (let x2 = 0; x2 < block_size; x2 ++) {
						
						switch(this.pixel) {
						case 0:
						case 1:
						case 2:
							let p = this.view.getUint16(ofs, true);
							this.ctx.fillStyle = this.color16(p);
							ofs += 2;
							break;
						case 3:
							let r = this.view.getUint8(ofs + 0);
							let g = this.view.getUint8(ofs + 1);
							let b = this.view.getUint8(ofs + 2);
							let a = this.view.getUint8(ofs + 3) / 255;
							this.ctx.fillStyle = "rgba("+r+","+g+","+b+","+a+")";
							ofs += 4;
							break;
						}	
						
						this.ctx.fillRect(x + x2, y + y2, 1, 1);

					}
				}
			}
		}

	}

	readIndex16() {
		
		const clut = new Array(16);
		let ofs = 0x20;

		for(let i = 0; i < clut.length; i++){
			switch(this.pixel) {
			case 0:
			case 1:
			case 2:
				let p = this.view.getUint16(ofs, true);
				clut[i] = this.color16(p);
				ofs += 2;
				break;
			case 3:
				let r = this.view.getUint8(ofs + 0);
				let g = this.view.getUint8(ofs + 1);
				let b = this.view.getUint8(ofs + 2);
				let a = this.view.getUint8(ofs + 3) / 255;
				clut[i] = "rgba("+r+","+g+","+b+","+a+")";
				ofs += 4;
			break;
			}					
		}

		for (let y = 0; y < this.height; y += 8) {
			for (let x = 0; x < this.width; x += 32) {
				for (let y2 = 0; y2 < 8; y2++) {
					for (let x2 = 0; x2 < 32; x2+=2) {
						
						let byte = this.view.getUint8(ofs++);
						this.ctx.fillStyle = clut[byte & 0x0f];
						this.ctx.fillRect(x + x2, y + y2, 1, 1);
						this.ctx.fillStyle = clut[byte >> 4];
						this.ctx.fillRect(x + x2 + 1, y + y2, 1, 1);

					}
				}
			}
		}


	}
	
	readIndex256 () {
		
		const clut = new Array(256);
		let ofs = 0x20;

		for(let i = 0; i < clut.length; i++){
			switch(this.pixel) {
			case 0:
			case 1:
			case 2:
				let p = this.view.getUint16(ofs, true);
				clut[i] = this.color16(p);
				ofs += 2;
				break;
			case 3:
				let r = this.view.getUint8(ofs + 0);
				let g = this.view.getUint8(ofs + 1);
				let b = this.view.getUint8(ofs + 2);
				let a = this.view.getUint8(ofs + 3) / 255;
				clut[i] = "rgba("+r+","+g+","+b+","+a+")";
				ofs += 4;
			break;
			}					
		}

		for (let y = 0; y < this.height; y += 8) {
			for (let x = 0; x < this.width; x += 16) {
				for (let y2 = 0; y2 < 8; y2++) {
					for (let x2 = 0; x2 < 16; x2++) {
						
						let byte = this.view.getUint8(ofs++);
						this.ctx.fillStyle = clut[byte];
						this.ctx.fillRect(x + x2, y + y2, 1, 1);

					}
				}
			}
		}

	}

	color16 (p) {

		const ARGB1555 = 0;
		const RGB565 = 1;
		const RGBA4444 = 2;
		const ARGB8888 = 3;

		let r, g, b, a;

		switch(this.pixel) {
		case ARGB1555:
			
			a = p & 0x8000 ? 1 : 0;
			r = ((p >> 0) & 0x1f) << 3;
			g = ((p >> 5) & 0x1f) << 3;
			b = ((p >> 10) & 0x1f) << 3;
			
			if(!a && p) {
				a = 0.5;
			}

			break;
		case RGB565:

			r = (p & 0x1f) << 3;
			g = ((p >> 5) & 0x3f) << 2;
			b = ((p >> 11) & 0x1f) << 3;
			a = 1;

			break;
		case RGBA4444:

			r = ((p >> 0) & 0xf) << 4;
			g = ((p >> 4) & 0xf) << 4;
			b = ((p >> 8) & 0xf) << 4;
			a = ((p >> 12) & 0xf) / 0xf;

			break;
		}
		
		return "rgba("+r+","+g+","+b+","+a+")";
	}

}
