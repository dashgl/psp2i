"use strict";

window.Assets = window.Assets || {};

window.Assets.Weapons = {

	"Saber" : async function() {

		const mod_req = await fetch('dat/IW_09_22.nbl');
		const mod_res = await mod_req.arrayBuffer();
		const mod_nbl = gasetools.nbl(mod_res);

		console.log(mod_nbl);

		// Read Bone Names

		const una = new UnaReader();
		const blist = una.parse(mod_nbl["wp_xxx_sa_nw_07_37_r.una"]);
		console.log(blist);

		const unj = new UnjReader(null, blist);
		let mesh = unj.parse(mod_nbl["wp_xxx_sa_nw_07_37_r.unj"]);

		const unm = new UnmReader(mesh);
		const key = "wp_xxx_sa_nw_07_37.unm"
		unm.parse(mod_nbl[key], key);

		SceneWidget.API.appendMesh(mesh);
	


		/*
		const blob = new Blob([mod_nbl["wp_xxx_sa_nw_07_37.unm"]]);
		saveAs(blob, "wp_xxx_sa_nw_07_37.unm");
		*/

		console.log(mod_nbl);

	}

}
