"use strict";

window.Assets = window.Assets || {};

window.Assets.Enemies = {

	"Booma" : async function() {
		
		// Get texture nbl
		const tex_req = await fetch('dat/su_en_booma_tn.nbl');
		const tex_res = await tex_req.arrayBuffer();
		const tex_nbl = gasetools.nbl(tex_res);

		// Get model nbl
		const mod_req = await fetch('dat/su_en_booma.nbl');
		const mod_res = await mod_req.arrayBuffer();
		const mod_nbl = gasetools.nbl(mod_res);

		console.log(mod_nbl);

		// Get model nbl
		const anim_req = await fetch('dat/su_en_humanoid_b_share.nbl');
		const anim_res = await anim_req.arrayBuffer();
		const anim_nbl = gasetools.nbl(anim_res);

		// Read Bone Names

		const una = new UnaReader();
		const blist = una.parse(mod_nbl["en_xxx_1mbma.una"]);

		const tlist = [];
		const uvr = new UvrReader();
		tlist.push(uvr.parse(tex_nbl["en_1mbm.uvr"]));

		const unj = new UnjReader(tlist, blist);
		let mesh = unj.parse(mod_nbl["en_xxx_1mbma.unj"]);

		console.log(anim_nbl);

		const unm = new UnmReader(mesh);
		let key = "en_xxx_die_aomuke_h_b.unm";
		//let key = "en_xxx_walk_h_b.unm";
		unm.parse(anim_nbl[key], key);

		SceneWidget.API.appendMesh(mesh);
		
		/*
		// GLTF
		let expt = new THREE.GLTFExporter();
		expt.parse(mesh, result => {
			
			const str = JSON.stringify(result);
			var blob = new Blob([str], {type: "text/plain;charset=utf-8"});
			saveAs(blob, "booma.gltf");

		}, {
			forceIndices : true
		});
		*/
		
		/*
		// Collada
		let expt = new THREE.ColladaExporter();
		let str = expt.parse(mesh);
		var blob = new Blob([str], {type: "text/plain;charset=utf-8"});
		saveAs(blob, "booma.dae");
		*/
		
		/*
		// Dash
		let dashExporter = new THREE.DashExporter();
		let blob = dashExporter.parse(mesh);
		saveAs(blob, "booma.dmf");
		*/
		
		/*
		// Direct
		//console.log(anim_nbl["en_xxx_walk_h_b.unm"]);
		const blob = new Blob([mod_nbl["en_xxx_1mbma.una"]]);
		saveAs(blob, "en_xxx_1mbma.una");
		*/
		

	}

}
