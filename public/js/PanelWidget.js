"use strict";

const PanelWidget = (function() {

	this.MEM = {}

	this.DOM = {}

	this.EVT = {}

	this.API = {}

	init.apply(this);
	return this;

	function init() {

		SceneWidget.API.resetScene();
		window.Assets.Weapons.Saber();

	}

}).apply({});
