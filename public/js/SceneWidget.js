"use strict";

const SceneWidget = (function() {

	this.MEM = {
		mixers : [],
		camera : new THREE.PerspectiveCamera(),
		clock : new THREE.Clock(),
		scene : new THREE.Scene(),
		grid : new THREE.GridHelper(50, 20, 0xff0000, 0x000),
		light : new THREE.AmbientLight( 0xffffff ),
		renderer : new THREE.WebGLRenderer({
			antialias: true,
			alpha : true,
			preserveDrawingBuffer: true
		})
	}

	this.DOM = {
		viewport : document.getElementById('SceneWidget.viewport')
	}

	this.EVT = {
		handleWindowResize : evt_handleWindowResize.bind(this)
	}

	this.API = {
		animate : api_animate.bind(this),
		updateViewportSize : api_updateViewportSize.bind(this),
		resetScene : api_resetScene.bind(this),
		appendMesh : api_appendMesh.bind(this)
	}

	init.apply(this);
	return this;

	function init() {

		// Initialize Threejs Attibutes

		this.MEM.camera.position.z = 25;
		this.MEM.camera.position.y = 15;
		this.MEM.camera.position.x = 0;
		this.MEM.camera.lookAt(new THREE.Vector3(0,0,0));

		this.MEM.renderer.domElement.style.margin = "0";
		this.MEM.renderer.domElement.style.padding = "0";
		this.MEM.renderer.setClearColor(0x000000, 0);

		this.MEM.scene.add(this.MEM.grid);
		this.MEM.scene.add(this.MEM.light);
		this.DOM.viewport.appendChild(this.MEM.renderer.domElement);
		THREE.OrbitControls(this.MEM.camera, this.MEM.renderer.domElement);

		// Add Event Listeners

		window.addEventListener("resize", this.EVT.handleWindowResize);

		// Call Init API functions

		this.API.updateViewportSize();
		this.API.animate();


	}

	function evt_handleWindowResize() {

		this.API.updateViewportSize();

	}

	function api_animate() {

		requestAnimationFrame(this.API.animate);
		this.MEM.renderer.render(this.MEM.scene, this.MEM.camera);

		const delta = this.MEM.clock.getDelta();
		this.MEM.mixers.forEach(mixer => {
			mixer.update(delta);
		});


	}

	function api_updateViewportSize() {

		const width = this.DOM.viewport.offsetWidth;
		const height = this.DOM.viewport.offsetHeight;

		this.MEM.camera.aspect = width / height;
		this.MEM.camera.updateProjectionMatrix();
		this.MEM.renderer.setSize( width, height);


	}
	
	function api_resetScene() {

		console.log("reset the scene!!!");

	}

	function api_appendMesh(mesh) {

		console.log("append de mesh!!");
		
		this.MEM.scene.add(mesh);
		if(mesh.type === "SkinnedMesh") {
			let helper = new THREE.SkeletonHelper( mesh );
			this.MEM.scene.add( helper );
		}

		if(!mesh.geometry.animations) {
			return;
		}
		
		let mixer = new THREE.AnimationMixer(mesh);
		let clip = mesh.geometry.animations[0];
		let action = mixer.clipAction(clip, mesh);
		action.timeScale = 1;
		action.play();
		this.MEM.mixers = [mixer];

	}

}).apply({});
