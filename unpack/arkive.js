/*
Copyright (c) 2019 Benjamin Collins - kion@dashgl.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

"use strict";

const fs = require("fs");
const path = require("path");

module.exports = (function(){

/**********************************************************
 * Library Function Definitions                           *
 **********************************************************/

	return {
        	"gsl" : gslExtract,
		"afs" : afsExtract,
		"bml" : bmlExtract,
		"prs" : prsDecompress,
		"prc" : prcDecompress
	};

/**********************************************************
 * Extract Files from AFS Arkive                          *
**********************************************************/

function gslExtract(gslArchive){

    var gsl = fs.readFileSync(gslArchive);

    var offset = 0;
    var gsl_header = [];
    var gsl_offset = gsl.length;
    var bigEndian = false;

    while(gsl.readUInt32LE(offset) && offset < gsl_offset) {

        var filename = gsl.toString("ascii", offset, offset + 0x20);

        var nullCharacterIndex = filename.indexOf('\0');
        if(nullCharacterIndex !== -1) {
            filename = filename.substr(0, nullCharacterIndex);
        }

        if(!offset){
            gsl_offset = gsl.readUInt32LE(offset + 0x20);

            if(gsl_offset > gsl.length){
                bigEndian = true;
                gsl_offset = gsl.readUInt32BE(offset + 0x20);
            }

            gsl_offset *= 2048;
        }

        var file_ofs, file_len;
        if(bigEndian){
            file_ofs = gsl.readUInt32BE(offset + 0x20) * 2048;
            file_len = gsl.readUInt32BE(offset + 0x24);
        }else{
            file_ofs = gsl.readUInt32LE(offset + 0x20) * 2048;
            file_len = gsl.readUInt32LE(offset + 0x24);
        }

        gsl_header.push({
            "name" : filename,
            "offset" : file_ofs,
            "length" : file_len
        });

        offset += 0x30;
    }

    for(let i = 0; i < gsl_header.length; i++){
        var entry = gsl_header[i];

        gsl_header[i]["data"] = new Buffer(entry.length);
        gsl.copy(gsl_header[i]["data"], 0, entry.offset, entry.offset + entry.length);

        delete gsl_header[i]["offset"];
        delete gsl_header[i]["length"];
    }

    return gsl_header;

}

/**********************************************************
 * Extract Files from AFS Arkive                          *
 **********************************************************/

	function afsExtract(afsArchive) {
		var afs = fs.readFileSync(afsArchive);

		var format = afs.toString("ascii", 0, 4);
		if(format != "AFS\0"){
			throw "Unknown AFS file format header: " + format;
		}

		var ext = path.extname(afsArchive);
		var basename = path.basename(afsArchive, ext);

		var entryList = [];
		var nbEntries = afs.readUInt32LE(4);
		for(let i = 8; i < (nbEntries + 1)*8; i += 8){
			entryList.push({
				"index" : entryList.length,
				"offset" : afs.readUInt32LE(i),
				"length" : afs.readUInt32LE(i + 4)
			});
		}

		var fileList = [];
		entryList.forEach(function(entry){

			var tmp = new Buffer(entry.length);
			afs.copy(tmp, 0, entry.offset, entry.offset + entry.length);
			tmp = prsDecompress(tmp);

			var num = entry.index.toString();
			while(num.length < 3){
				num = "0" + num;
			}

			switch(tmp.toString("ascii", 0, 4)){
				case "NJIN":
					//Ninja Information
					ext = ".nji";
				break;
				case "NJCM":
				case "NJBM":
				case "NJTL":
					//Ninja Model
					ext = ".nj";
				break;
				case "NJLI":
					//Ninja Light
					ext = ".njl";
				break;
				case "NJCA":
					//Ninja Camera
					ext = ".njc";
				break;
				case "NMDM":
					//Ninja Motion
					ext = ".njm"
				break;
				case "NLIM":
					//Ninja Light Motions
					ext = ".nlm"
				break;
				case "NCAM":
					//Ninja Camera Motion
					ext = ".ncm";
				break;
				case "NSSM":
					//Ninja Shape Motion
					ext = ".nsm";
				break;
				case "NJSP":
					//Ninja Sprite
					ext = ".nsp";
				break;
				case "PVRT":
				case "GBIX":
					//PowerVR Texture
					ext = ".pvr";
				break;
				case "PVMH":
					//PowerVR Archive
					ext = ".pvm";
				break;
				case "GJCM":
				case "GJBM":
				case "GJTL":
					//Ninja Model
					ext = ".gj";
				break;
				case "GVRT":
					//PowerVR Texture
					ext = ".gvr";
				break;
				case "GVMH":
					//PowerVR Archive
					ext = ".gvm";
				break;
				default:
					//Default Unknown binary
					ext = ".bin";
				break;
			}

			fileList.push({
				"name" : basename + "_" + num + ext,
				"data" : tmp
			});

		});

		return fileList;
	}

/**********************************************************
 * Extract Files from BML Arkive                          *
 **********************************************************/

	function bmlExtract(bmlArchive) {

		var bml;
		if(Buffer.isBuffer(bmlArchive)){
			bml = bmlArchive;
		} else {
			bml = fs.readFileSync(bmlArchive);
		}

		var isBigEndian = false;
		var nbItems = bml.readUInt16LE(4);

		if(!nbItems){
			isBigEndian = true;
			nbItems = bml.readUInt16BE(6);
		}

		var ofs = 0x40;
		var index = [];
		for(let i = 0; i < nbItems; i++) {

			var item = {};
			item.filename = bml.toString("ascii", ofs, ofs + 0x20);
			item.filename = item.filename.replace(/\0/g, "");

			if(!isBigEndian){
				item.compressedLength = bml.readUInt32LE(ofs + 0x20);
				item.decompressedLength = bml.readUInt32LE(ofs + 0x28);
				item.pvmCompressedLength = bml.readUInt32LE(ofs + 0x2C);
				item.pvmDecompressedLength = bml.readUInt32LE(ofs + 0x30);
			}else {
				item.compressedLength = bml.readUInt32BE(ofs + 0x20);
				item.decompressedLength = bml.readUInt32BE(ofs + 0x28);
				item.pvmCompressedLength = bml.readUInt32BE(ofs + 0x2C);
				item.pvmDecompressedLength = bml.readUInt32BE(ofs + 0x30);
			}

			ofs += 0x40;
			index.push(item);

		}

		if (ofs < 0x800) {
			ofs = 0x800;
		} else {
			seekNextByte();
			ofs = 0xFFFFF800 & ofs;
		}

		var filelist = [];
		for(let i = 0; i < nbItems; i++){

			var item = index[i];
			var buffer = new Buffer(item.compressedLength);
			bml.copy(buffer, 0, ofs, ofs + item.compressedLength);
			buffer = prsDecompress(buffer);

			filelist.push({
				"name" : item.filename,
				"data" : buffer
			});

			ofs += item.compressedLength;
			seekNextByte();

			if(!item.pvmCompressedLength){
				continue;
			}

			var item = index[i];
			var buffer = new Buffer(item.pvmCompressedLength);
			bml.copy(buffer, 0, ofs, ofs + item.pvmCompressedLength);
			buffer = prsDecompress(buffer);

			var texname = item.filename.split(".")[0];
			if(!isBigEndian){
				texname += ".pvm";
			}else{
				texname += ".gvm";
			}

			filelist.push({
				"name" : texname,
				"data" : buffer
			});

			ofs += item.pvmCompressedLength;
			seekNextByte();
		}

		return filelist;

		function seekNextByte(){

			while(ofs < bml.length){

				if(bml[ofs]){
					break;
				}

				ofs++;
			}

		}

	}

/**********************************************************
 * Decompress PRS (LZ-77) Encoded Buffer                  *
 **********************************************************/

	function prsDecompress(data) {

		var self = {
			ibuf: data,
			obuf: [],
			iofs: 0,
			bit: 0,
			cmd: 0,
			getByte: int_getByte,
			getBit: bool_getBit
		}

		var t, a, b, j, cmd;
		var offset, amount, start;

		while (self.iofs < self.ibuf.length) {
			cmd = self.getBit();
			if (cmd) {
				self.obuf.push(self.ibuf[self.iofs]);
				self.iofs += 1;
			} else {
				t = self.getBit();
				if (t) {
					a = self.getByte();
					b = self.getByte();

					offset = ((b << 8) | a) >> 3;
					amount = a & 7;
					if (self.iofs < self.ibuf.length) {
						if (amount == 0)
							amount = self.getByte() + 1;
						else
							amount += 2;
					}
					start = self.obuf.length - 0x2000 + offset;
				} else {
					amount = 0;
					for (j = 0; j < 2; j++) {
						amount <<= 1;
						amount |= self.getBit();
					}
					offset = self.getByte();
					amount += 2;

					start = self.obuf.length - 0x100 + offset;
				}
				for (j = 0; j < amount; j++) {
					if (start < 0)
						self.obuf.push(0);
					else if (start < self.obuf.length)
						self.obuf.push(self.obuf[start]);
					else
						self.obuf.push(0);

					start += 1;
				}
			}
		} //end while

		return new Buffer(self.obuf);

		function int_getByte() {
			var val = self.ibuf[self.iofs];
			self.iofs += 1;
			return parseInt(val);
		}

		function bool_getBit() {
			if (self.bit == 0) {
				self.cmd = self.getByte()
				self.bit = 8
			}
			var bit = self.cmd & 1;
			self.cmd >>= 1;
			self.bit -= 1;
			return parseInt(bit);
		}
	} //end function

/**********************************************************
 * Decompress PRC Encrypted Buffer                        *
 **********************************************************/

	function prcDecompress(buffer) {
		var cxt = {
			"stream": new Uint32Array(56),
			"key": null,
			"pos": null
		};

		var buf = [];

		for (var i = 0; i < 8; i++) {
			buf[i] = buffer[i];
		}
		buffer = buffer.slice(8);

		var unc_len = buf[0] | (buf[1] << 8) | (buf[2] << 16) | (buf[3] << 24);
		var key = buf[4] | (buf[5] << 8) | (buf[6] << 16) | (buf[7] << 24);

		//pso_prsd_crypt_init
		cxt.stream[55] = key;
		cxt.key = key;

		var idx;
		var tmp = 1;

		for (var i = 0x15; i <= 0x46E; i += 0x15) {
			idx = i % 55;
			key -= tmp;
			cxt.stream[idx] = tmp;
			tmp = key;
			key = cxt.stream[idx];
		}

		mix_stream(cxt);
		mix_stream(cxt);
		mix_stream(cxt);
		mix_stream(cxt);

		cxt.pos = 56;

		//pso_prsd_crypt
		var tmp, pos, dword;
		var cmp_buf = buffer;
		var len = cmp_buf.length + 3;
		len = len & 0xFFFFFFFC;

		pos = 0;
		while (len > 0) {
			dword = cmp_buf.readUInt32LE(pos);
			tmp = crypt_dword(cxt, dword);
			cmp_buf.writeUInt32LE(tmp, pos);

			pos += 4;
			len -= 4;
		}

		var dst = prs_decompress(cmp_buf);
		return dst;

		function mix_stream(cxt) {
			var ptr;

			ptr = 1;
			for (let i = 24; i; --i, ++ptr) {
				cxt.stream[ptr] -= cxt.stream[ptr + 31];
			}

			ptr = 25;
			for (let i = 31; i; --i, ++ptr) {
				cxt.stream[ptr] -= cxt.stream[ptr - 24];
			}
		}

		function crypt_dword(cxt, data) {
			if (cxt.pos === 56) {
				mix_stream(cxt);
				cxt.pos = 1;
			}

			var res = new Uint32Array(1);
			res[0] = data ^ cxt.stream[cxt.pos++];

			return res[0];
		}
	} //end function

}());
