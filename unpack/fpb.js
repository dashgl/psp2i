/*
 * @author - Benjamin Collins - kion@dashgl.com
 * Adapted from : https://github.com/essen/gasetools
 * Credit : JamRules
 * a) Added BIND, RIFF Magic number to fpb unpack
 * License : GPL 3.0
 */

"use strict";

const fs = require('fs');
console.log(process.argv);

if(process.argv.length !== 3) {
	console.error("Usage: node %s file.fpb", process.argv[1]);
	process.exit();
}

let ext = process.argv[2].split(".").pop();
if(ext !== "fpb") {
	console.error("Usage: node %s file.fpb", process.argv[1]);
	process.exit();
}

try {
	var src = fs.readFileSync(process.argv[2]);
} catch(err) {
	console.error("Error: could not open %s for reading", process.argv[2]);
	process.exit();
}

const FPB_NMLL = 0x4c4c4d4e;
const FPB_RIFF = 0x46464952;
const FPB_BIND = 0x444e4942;

const files = [];
for(let i = 0; i < src.length - 4; i+= 0x10) {

	let dword = src.readUInt32LE(i);
	switch(dword) {
	case FPB_NMLL:
	case FPB_RIFF:
	case FPB_BIND:
		files.push({
			magic : dword,
			offset : i
		});
	}

}

files.push({
	offset : src.length
});

if(!fs.existsSync("out_fpb")) {
	fs.mkdirSync("out_fpb");
} else if(!fs.lstatSync("out_fpb").isDirectory()) {
	console.error("out_fpb/ could not be created");
	process.exit();
}

let index = 0;
for(let i = 0; i < files.length - 1; i++) {

	if(files[i].magic !== FPB_NMLL) {
		continue;
	}

	let dst = src.subarray(files[i].offset, files[i + 1].offset);
	let num = index.toString();
	while(num.length < 5) {
		num = "0" + num;
	}
	
	let name = "out_fpb/nmll-"+num+".nbl";
	if(dst[5]) {
		name = "out_fpb/nmll-"+num+"_new.nbl";
	}

	fs.writeFileSync(name, dst);
	
	index++;

}
