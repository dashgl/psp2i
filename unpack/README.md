# Unpack Scripts

This folder includes scripts that are used for unpacking the file.fpb
archive which contains the assets for Phantasy Star Portable 2 Infinity.
These scripts are written in Nodejs and can be run from the command line
assuming you have Nodejs installed on your system.

## fpb.js

The fpb.js script is a script that will preform a quick and dirty export
of all of the files contained within the file.fpb archive. The fpb archive
itself contains three types of top level file types that are defined by
the magic numbers NMLL, RIFF and BIND respectively. These files are stacked
end to end inside the file. As such the way this script works is to scan
through the file to find the respective offsets of each instance of these
magic numbers at whole multiples of 0x10, create a folder names /fbp_out
inside this directory, and then export all of the files into that directory.

```$ node fpb.js file.fpb```

## nbl.js

The nbl.js script is a script that will scan the /out_fpb folder for
.nbl archives. When it finds an .nbl file it will attempt to extract 
the asset files from the nbl acrhive, create a folder named /out_nbl
if it doesn't exist, and then write the resultling files to that directory. 

```$ node nbl.js```

## uvr.js

The uvr.js script is a script that will scan the /out_nbl directory
for .uvr files, which are the texture format for psp2i and attempt to
parse and export them as png files to /out_uvr. The directory will be
created if it doesn't exist. To run this script you will need to install
the pngjs module before the first run.

```
$ npm install pngjs
$ node uvr.js
```

### fakefish.js

The fakefish.js script doesn't have any functionality on it's own. It
acts as a library for Sega's custom blowfish implementation.

