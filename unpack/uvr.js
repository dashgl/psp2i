"use strict";

const fs = require('fs');
const PNG = require('pngjs').PNG;

if(!fs.existsSync("out_nbl")) {
    console.error("please run nbl.js before runing this script");
    process.exit();
} else if(!fs.lstatSync("out_nbl").isDirectory()) {
    console.error("out_nbl is not a directory, cannot scan");
    process.exit();
}

const MAGIC_GBIX = 0x58494247;
const MAGIC_UVRT = 0x54525655;

const PIXEL_RGBA8888 = 3;
const IMAGE_4BITINDEX = 0x88;

const dir = fs.readdirSync('out_nbl');
dir.forEach(file => {

	let ext = file.split('.').pop();
	if(ext !== 'uvr') {
		return;
	}

	const fp = fs.readFileSync('out_nbl/' + file);
	const gbix = fp.readUInt32LE(0x00);
	const uvrt = fp.readUInt32LE(0x10);

	if(gbix !== MAGIC_GBIX || uvrt !== MAGIC_UVRT) {
		return;
	}

	const pixel_format = fp.readUInt8(0x18);
	const image_format = fp.readUInt8(0x19);
	const format = fp.readUInt16BE(0x18);
	const width = fp.readUInt16LE(0x1c);
	const height = fp.readUInt16LE(0x1e);

	const data = fp.subarray(0x20, fp.length);

	let png;
	switch(format) {
	case 0x18a:
		png = read8BitIndex(width, height, data);
		break;
	case 0x389:
		png = read4BitIndexA(width, height, data);
		break;
	case 0x181:
		png = readRaw16(width, height, data);
		break;
	case 0x8a:
		png = read4BitIndex16C(width, height, data);
		break;
	case 0x380:
		png = readRaw32(width, height, data);
		break;
	case 0x38c:
		png = read8BitIndex256(width, height, data);
		break;
	case 0x286:
		png = read4BitIndex16RGBA444(width, height, data);
		break;
	case 0x81:
		png = readRawARGB1555(width, height, data);
		break;
	case 0x388:
		png = read4BitIndex(width, height, data);
		break;
	case 0x186:
		png = read4BitIndex16(width, height, data);
		break;
	case 0x86:
		png = read4BitIndex16b(width, height, data);
		break;
	case 0x180:
		png = readRaw16(width, height, data);
		break;
	default:

		console.log("");
		console.log("Texture: %s", file);
		console.log("Format Short: 0x%s", format.toString(16));	
		console.log("Combination not yet supported");
		console.log("");

		break;
	}

	if(!png) {
		return;
	}

	console.log("Exporting Texture: %s", file);
	console.log("Format Short: 0x%s", format.toString(16));	
	console.log("");

    if(!fs.existsSync("out_uvr")) {
        fs.mkdirSync("out_uvr");
    } else if(!fs.lstatSync("out_uvr").isDirectory()) {
        console.error("out_uvr/ could not be created");
        process.exit();
    }

	let buffer = PNG.sync.write(png);
	let name = file.split(".").shift() + ".png";
	fs.writeFileSync("out_uvr/" + name, buffer);

});

console.log("Finished");

function read4BitIndex(width, height, data) {

	let ofs = 0;
	const clut = new Array(16);
	for(let i = 0; i < 16; i++) {
		clut[i] = data.readUInt32LE(ofs);
		ofs += 4;
	}

	let png = new PNG({
		width : width,
		height : height,
		data : new Array()
	});

	console.log("Png Size: 0x%s", png.data.length);

	for (let y = 0; y < height; y += 8) {
		for (let x = 0; x < width; x += 32) {
			for (let y2 = 0; y2 < 8; y2++) {
				for (let x2 = 0; x2 < 32; x2 += 2) {

					let byte = data.readUInt8(ofs++);
					let a = ((((y + y2) * width) + (x + x2 + 0)) * 4);
					let b = ((((y + y2) * width) + (x + x2 + 1)) * 4);
					
					png.data.writeUInt32LE(clut[byte & 0x0f], a);
					png.data.writeUInt32LE(clut[byte >> 4], b);
                    
				}
			}
		}
	}

	return png;

}

function read4BitIndex16(width, height, data) {

	let ofs = 0;
	const clut = new Uint32Array(16);
	for(let i = 0; i < 16; i++) {
		let p = data.readUInt16LE(ofs);

		let r = (p & 0x1f) << 3;
		let g = ((p >> 5) & 0x3f) << 2;
		let b = ((p >> 11) & 0x1f) << 3;
		let a = 0xff;

		clut[i] = 0xff000000|r<<16|g<<8|b;
		ofs += 2;
	}

	let png = new PNG({
		width : width,
		height : height,
		data : new Array()
	});

	for (let y = 0; y < height; y += 8) {
		for (let x = 0; x < width; x += 32) {
			for (let y2 = 0; y2 < 8; y2++) {
				for (let x2 = 0; x2 < 32; x2 += 2) {

					let byte = data.readUInt8(ofs++);
					let a = ((((y + y2) * width) + (x + x2 + 0)) * 4);
					let b = ((((y + y2) * width) + (x + x2 + 1)) * 4);
					
					png.data.writeUInt32LE(clut[byte & 0x0f], a);
					png.data.writeUInt32LE(clut[byte >> 4], b);
                    
				}
			}
		}
	}

	return png;

}

function read4BitIndex16b(width, height, data) {

	let ofs = 0;
	const clut = new Uint32Array(16);
	for(let i = 0; i < 16; i++) {
		let p = data.readUInt16LE(ofs);

		let a = p & 0x8000;
		let r = ((p >> 10) & 0x1f) << 3;
		let g = ((p >> 5) & 0x1f) << 3;
		let b = ((p >> 0) & 0x1f) << 3;
		
		if(a) {
			a = 0xff;
		} else if(!a && (r|g|b)) {
			a = 0x80;
		} else {
			a = 0;
		}

		clut[i] = a<<24|r<<16|g<<8|b;
		ofs += 2;
	}

	let png = new PNG({
		width : width,
		height : height,
		data : new Array()
	});

	for (let y = 0; y < height; y += 8) {
		for (let x = 0; x < width; x += 32) {
			for (let y2 = 0; y2 < 8; y2++) {
				for (let x2 = 0; x2 < 32; x2 += 2) {

					let byte = data.readUInt8(ofs++);
					let a = ((((y + y2) * width) + (x + x2 + 0)) * 4);
					let b = ((((y + y2) * width) + (x + x2 + 1)) * 4);
					
					png.data.writeUInt32LE(clut[byte & 0x0f], a);
					png.data.writeUInt32LE(clut[byte >> 4], b);
                    
				}
			}
		}
	}

	return png;

}

function readRaw16(width, height, data) {

	const color = new Uint32Array(1);
	let png = new PNG({
		width : width,
		height : height,
		data : new Array()
	});

	let ofs = 0;

	for (let y = 0; y < height; y += 8) {
		for (let x = 0; x < width; x += 8) {
			for (let y2 = 0; y2 < 8; y2++) {
				for (let x2 = 0; x2 < 8; x2 ++) {
					
					if(x2 >= width-1) {
						continue;
					}

					let p = data.readUInt16LE(ofs);
					ofs += 2;
					let idx = ((((y + y2) * width) + (x + x2)) * 4);

					let r = (p & 0x1f) << 3;
					let g = ((p >> 5) & 0x3f) << 2;
					let b = ((p >> 11) & 0x1f) << 3;
					let a = 0xff;
					color[0] = 0xff000000|r<<16|g<<8|b;
					
					png.data.writeUInt32LE(color[0], idx);
                    
				}
			}
		}
	}

	return png;

}

function read8BitIndex(width, height, data) {

	let ofs = 0;
	const clut = new Uint32Array(256);
	for(let i = 0; i < 256; i++) {
		let p = data.readUInt16LE(ofs);

		let r = (p & 0x1f) << 3;
		let g = ((p >> 5) & 0x3f) << 2;
		let b = ((p >> 11) & 0x1f) << 3;
		let a = 0xff;

		clut[i] = 0xff000000|r<<16|g<<8|b;
		ofs += 2;
	}

	let png = new PNG({
		width : width,
		height : height,
		data : new Array()
	});

	for (let y = 0; y < height; y += 8) {
		for (let x = 0; x < width; x += 16) {
			for (let y2 = 0; y2 < 8; y2++) {
				for (let x2 = 0; x2 < 16; x2 ++) {

					let byte = data.readUInt8(ofs++);
					let a = ((((y + y2) * width) + (x + x2)) * 4);
					
					png.data.writeUInt32LE(clut[byte], a);
                    
				}
			}
		}
	}

	return png;

}

function read4BitIndexA(width, height, data) {

	let ofs = 0;
	const clut = new Array(16);
	for(let i = 0; i < 16; i++) {
		clut[i] = data.readUInt32LE(ofs);
		ofs += 4;
	}

	let png = new PNG({
		width : width,
		height : height,
		data : new Array()
	});

	// ofs = data.length - (width*height/2);

	console.log("Png Size: 0x%s", png.data.length);

	for (let y = 0; y < height; y += 8) {
		for (let x = 0; x < width; x += 32) {
			for (let y2 = 0; y2 < 8; y2++) {
				for (let x2 = 0; x2 < 32; x2 += 2) {

					let byte = data.readUInt8(ofs++);
					let a = ((((y + y2) * width) + (x + x2 + 0)) * 4);
					let b = ((((y + y2) * width) + (x + x2 + 1)) * 4);
					
					png.data.writeUInt32LE(clut[byte & 0x0f], a);
					png.data.writeUInt32LE(clut[byte >> 4], b);
                    
				}
			}
		}
	}

	return png;

}

function read4BitIndex16C(width, height, data) {

	let ofs = 0;
	const clut = new Uint32Array(256);
	for(let i = 0; i < 256; i++) {
		let p = data.readUInt16LE(ofs);

		let a = p & 0x8000;
		let r = ((p >> 10) & 0x1f) << 3;
		let g = ((p >> 5) & 0x1f) << 3;
		let b = ((p >> 0) & 0x1f) << 3;
		
		if(a) {
			a = 0xff;
		} else if(!a && (r|g|b)) {
			a = 0x80;
		} else {
			a = 0;
		}

		clut[i] = a<<24|r<<16|g<<8|b;
		ofs += 2;
	}

	let png = new PNG({
		width : width,
		height : height,
		data : new Array()
	});

	for (let y = 0; y < height; y += 8) {
		for (let x = 0; x < width; x += 16) {
			for (let y2 = 0; y2 < 8; y2++) {
				for (let x2 = 0; x2 < 16; x2++) {

					let byte = data.readUInt8(ofs++);
					let a = ((((y + y2) * width) + (x + x2)) * 4);
					png.data.writeUInt32LE(clut[byte], a);
                    
				}
			}
		}
	}

	return png;

}

function readRaw32(width, height, data) {

	const color = new Uint32Array(1);
	let png = new PNG({
		width : width,
		height : height,
		data : new Array()
	});

	let ofs = 0;

	for (let y = 0; y < height; y += 8) {
		for (let x = 0; x < width; x += 4) {
			for (let y2 = 0; y2 < 8; y2++) {
				for (let x2 = 0; x2 < 4; x2++) {
					
					color[0] = data.readUInt32LE(ofs);
					ofs += 4;
					let idx = ((((y + y2) * width) + (x + x2)) * 4);
					png.data.writeUInt32LE(color[0], idx);
                    
				}
			}
		}
	}

	return png;


}

function readRawARGB1555(width, height, data) {

	const color = new Uint32Array(1);
	let png = new PNG({
		width : width,
		height : height,
		data : new Array()
	});

	let ofs = 0;

	for (let y = 0; y < height; y += 8) {
		for (let x = 0; x < width; x += 8) {
			for (let y2 = 0; y2 < 8; y2++) {
				for (let x2 = 0; x2 < 8; x2 ++) {
					
					if(x2 >= width-1) {
						continue;
					}

					let p = data.readUInt16LE(ofs);
					ofs += 2;
					let idx = ((((y + y2) * width) + (x + x2)) * 4);

					let a = p & 0x8000;
					let r = ((p >> 10) & 0x1f) << 3;
					let g = ((p >> 5) & 0x1f) << 3;
					let b = ((p >> 0) & 0x1f) << 3;
						
					if(a) {
						a = 0xff;
					} else if(!a && (r|g|b)) {
						a = 0x80;
					} else {
						a = 0;
					}

					color[0] = a<<24|r<<16|g<<8|b;
					png.data.writeUInt32LE(color[0], idx);
                    
				}
			}
		}
	}

	return png;

}

function read4BitIndex16RGBA444(width, height, data) {

	let ofs = 0;
	const clut = new Uint32Array(16);
	for(let i = 0; i < 16; i++) {
		let p = data.readUInt16LE(ofs);

		let r = ((p >> 0) & 0xf) << 4;
		let g = ((p >> 4) & 0xf) << 4;
		let b = ((p >> 8) & 0xf) << 4;
		let a = ((p >> 12) & 0xf) << 4;
		
		clut[i] = a<<24|r<<16|g<<8|b;
		ofs += 2;
	}

	let png = new PNG({
		width : width,
		height : height,
		data : new Array()
	});

	for (let y = 0; y < height; y += 8) {
		for (let x = 0; x < width; x += 32) {
			for (let y2 = 0; y2 < 8; y2++) {
				for (let x2 = 0; x2 < 32; x2 += 2) {

					let byte = data.readUInt8(ofs++);
					let a = ((((y + y2) * width) + (x + x2 + 0)) * 4);
					let b = ((((y + y2) * width) + (x + x2 + 1)) * 4);
					
					png.data.writeUInt32LE(clut[byte & 0x0f], a);
					png.data.writeUInt32LE(clut[byte >> 4], b);
                    
				}
			}
		}
	}

	return png;

}

function read8BitIndex256(width, height, data) {

	let ofs = 0;
	const clut = new Uint32Array(256);
	for(let i = 0; i < 256; i++) {
		clut[i] = data.readUInt32LE(ofs);
		ofs += 4;
	}

	let png = new PNG({
		width : width,
		height : height,
		data : new Array()
	});

	for (let y = 0; y < height; y += 8) {
		for (let x = 0; x < width; x += 16) {
			for (let y2 = 0; y2 < 8; y2++) {
				for (let x2 = 0; x2 < 16; x2 ++) {

					let byte = data.readUInt8(ofs++);
					let a = ((((y + y2) * width) + (x + x2)) * 4);
					png.data.writeUInt32LE(clut[byte], a);
                    
				}
			}
		}
	}

	return png;

}
