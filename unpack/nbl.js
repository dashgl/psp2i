/*
 * @author - Benjamin Collins - kion@dashgl.com
 * Adapted from : https://github.com/essen/gasetools
 * License : GPL 3.0
 */

"use strict";

const fs = require('fs');
const arkive = require('./arkive.js');
const FakeFish = require("./fakefish.js");

console.log(arkive);

const MAGIC_NMLL = 0x4C4C4D4E;
const MAGIC_TMLL = 0x4C4C4D54;


if(!fs.existsSync("out_fpb")) {
	console.error("please run fbp.js before runing this script");
	process.exit();
} else if(!fs.lstatSync("out_fpb").isDirectory()) {
	console.error("out_fpb is not a directory, cannot scan");
	process.exit();
}

const dir = fs.readdirSync("out_fpb");
dir.forEach(file => {

	console.log(file);

	let ext = file.split(".").pop();
	if(ext !== "nbl") {
		return;
	}

	const fp = fs.readFileSync("out_fpb/" + file);

	// Skip NBL type 2 for right now

	if(fp[5]) {
		return;
	}

	// Read the nbl header

	const header = {
		magic : fp.readUInt32LE(0x00),
		unknown : fp.readUInt32LE(0x04),
		nmll : {
			header_size : fp.readUInt32LE(0x08),
			chunk_count : fp.readUInt32LE(0x0c),
			full_length : fp.readUInt32LE(0x10),
			comp_length : fp.readUInt32LE(0x14)
		},
		pointer_table : fp.readUInt32LE(0x18),
		key_seed : fp.readUInt32LE(0x1c),
		tmll : {
			header_size : fp.readUInt32LE(0x20),
			full_length : fp.readUInt32LE(0x24),
			comp_length : fp.readUInt32LE(0x28),
			chunk_count : fp.readUInt32LE(0x2c)
		}
	};
	
	if(header.magic !== MAGIC_NMLL) {
		return;
	}

	const isNmllCompr = fp.readUInt32LE(0x14);
	const isEncrypted = fp.readUInt32LE(0x1c);
	
	const key = new Uint8Array(4);
	key[0] = fp[0x1C + 3];
	key[1] = fp[0x1C + 2];
	key[2] = fp[0x1C + 1];
	key[3] = fp[0x1C + 0];
	const fish = new FakeFish(key);

	// Decrypt header
	
	console.log("1. Decrypting header");

	let ofs = 0x40;
	if(isEncrypted) {
		for(let i = 0; i < header.nmll.chunk_count; i++) {
			for(let k = ofs; k < ofs + 0x30; k += 8) {
				fish.bf_decrypt(fp, k);
			}
			ofs += 0x60;
		}
	}

	// Read Files from header

	console.log("2. Reading files from header");

	ofs = 0x40;
	const iNmllFiles = new Array(header.nmll.chunk_count);
	for(let i = 0; i < header.nmll.chunk_count; i++) {
		iNmllFiles[i] = {
			name : fp.toString('ascii', ofs, ofs + 0x20).replace(/\0/g, ''),
			start : fp.readUInt32LE(ofs + 0x20),
			length : fp.readUInt32LE(ofs + 0x24)
		}
		ofs += 0x60;
		console.log(iNmllFiles[i].name);
	}

	// Find Start of Payload

	console.log("3. Finding start of payload");

	ofs = header.nmll.header_size;
	while(fp.readUInt32LE(ofs) === 0) {
		ofs += 0x10;
		if(ofs < fp.length - 4) {
			continue;
		}
		console.log("Coult not find start of payload");
		return;
	}

	// Decrypt nmll payload

	console.log("4. Decrypting the files");

	const iNmllOfs = ofs;
	const iNmllSize = header.nmll.comp_length || header.nmll.full_length;
	if(isEncrypted) {
		for(let i = iNmllOfs; i < iNmllOfs + iNmllSize; i += 8) {
			fish.bf_decrypt(fp, i);
		}
	}

	// Decompress nmll payload

	console.log("5. Decompressing the files");

	let iNmllData;
	if(!isNmllCompr) {
		let full_len = header.nmll.full_length;
		iNmllData = fp.subarray(iNmllOfs, iNmllOfs + full_len);
	} else {
		let comp_len = header.nmll.comp_length;
		let full_len = header.nmll.full_length;
		console.log("Doing the thing: 0x%s", full_len.toString(16));
		let src = fp.subarray(iNmllOfs, iNmllOfs + comp_len);
		//iNmllData = nbl_decompress(src, full_len);
		iNmllData = arkive.prs(src);
	}

	// Check for out directory

	if(!fs.existsSync("out_nbl")) {
		fs.mkdirSync("out_nbl");
	} else if(!fs.lstatSync("out_nbl").isDirectory()) {
		console.error("out_nbl/ could not be created");
		process.exit();
	}

	console.log("6. Exporting the files");

	// Export nmll files

	iNmllFiles.forEach(file => {
		let data = iNmllData.subarray(file.start, file.start + file.length);
		fs.writeFileSync("out_nbl/" + file.name, data);
	});

	// Check for TMLL section
	ofs = iNmllOfs + iNmllSize;
	ofs += 0x10 - (ofs % 0x10);
	let found = false;
	for(ofs; ofs < fp.length - 4; ofs += 0x10) {
		let magic = fp.readUInt32LE(ofs);
		if(magic !== MAGIC_TMLL) {
			continue;
		}
		found = true;
		break;
	}
	
	console.log("7. TMLL found at: 0x%s", ofs.toString(16));

	if(!found) {
		return;
	}

	ofs += 0x30;
	const iTmllHeader = ofs;
	if(isEncrypted) {
		for(let i = 0; i < header.tmll.chunk_count; i++) {
			for(let k = ofs; k < ofs + 0x30; k += 8) {
				fish.bf_decrypt(fp, k);
			}
			ofs += 0x60;
		}
	}

	ofs = iTmllHeader;
	const iTmllFiles = new Array(header.tmll.chunk_count);
	for(let i = 0; i < header.tmll.chunk_count; i++) {
		iTmllFiles[i] = {
			name : fp.toString('ascii', ofs, ofs + 0x20).replace(/\0/g, ''),
			start : fp.readUInt32LE(ofs + 0x20),
			length : fp.readUInt32LE(ofs + 0x24)
		}
		ofs += 0x60;
	}

	// Seek to first non-zero offset

	while(fp.readUInt32LE(ofs) === 0) {
		ofs += 0x10;
	}

	console.log("8. Current position: 0x%s", ofs.toString(16));

	// Decompress the Tmll Payload

	let iTmllData;
	if(!header.tmll.comp_length) {
		let full_len = header.tmll.full_length;
		iTmllData = fp.subarray(ofs, ofs + full_len);
	} else {
		let comp_len = header.tmll.comp_length;
		let full_len = header.tmll.full_length;
		let src = fp.subarray(ofs, ofs + comp_len);
		//iTmllData = nbl_decompress(src, full_len);
		iTmllData = arkive.prs(src);
	}

	// Export Tmll Files

	iTmllFiles.forEach(file => {
		let data = iTmllData.subarray(file.start, file.start + file.length);
		fs.writeFileSync("out_nbl/" + file.name, data);
	});
	
	fs.writeFileSync("debug_b.bin", iTmllData);
	fs.writeFileSync("debug.bin", fp);

});


function nbl_decompress_get_next_control_bit(p){
    let ret;

    p.uControlByteCounter[0]--;
    if (p.uControlByteCounter[0] === 0) {
        p.ucControlByte[0] = p.pstrSrc[p.iSrcPos++];
        p.uControlByteCounter[0] = 8;
    }

    ret = p.ucControlByte[0] & 0x1;
    p.ucControlByte[0] >>= 1;

    return ret;
}


function nbl_decompress(pstrSrc, iDestSize) {

    let iDestPos, iTmpCount, iTmpPos, a, b;
    const pstrDest = Buffer.alloc(iDestSize, 0);
    
	iDestPos = 0;

	const p = {
		uControlByteCounter : new Uint32Array([1]),
		ucControlByte : new Uint8Array([0]),
		pstrSrc : pstrSrc,
		iSrcPos : 0
	}

    while (1) {
        /* Step 1: Write uncompressed data directly */

        while (nbl_decompress_get_next_control_bit(p)) {
            pstrDest[iDestPos++] = p.pstrSrc[p.iSrcPos++];
        }

        /* Step 2: Calculate the two values used in step 3 */

        if (nbl_decompress_get_next_control_bit(p)) {
            iTmpCount = p.pstrSrc[p.iSrcPos++];
            iTmpPos   = p.pstrSrc[p.iSrcPos++];

            if (iTmpCount == 0 && iTmpPos == 0) {
				break;
			}

            iTmpPos = (iTmpPos << 5) + (iTmpCount >> 3) - 0x2000;
            iTmpCount &= 7;

            if (iTmpCount == 0) {
                iTmpCount = p.pstrSrc[p.iSrcPos++] + 1;
            } else {
                iTmpCount += 2;
			}

        } else {
            a = nbl_decompress_get_next_control_bit(p);
            b = nbl_decompress_get_next_control_bit(p);

            iTmpCount = b + a * 2 + 2;
            iTmpPos = p.pstrSrc[p.iSrcPos++] - 0x100;
        }

        iTmpPos += iDestPos;

        /* Step 3: Use those values to retrieve what we want from the output buffer */

        while (iTmpCount > 0) {
            iTmpCount--;
            pstrDest[iDestPos++] = pstrDest[iTmpPos++];
        }

    }

	return pstrDest;

}

