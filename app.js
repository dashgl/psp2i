"use strict";

const fs = require('fs');
const express = require('express');
const app = express();
const port = 8080;

app.use(express.static('public'));

app.listen(port, function(){
	console.log('Server is listening on port 8080');
});
